<?php
/**
 * @file
 * Reduced Forms administration functions.
 */

/**
 * Form constructor for Reduced Forms settings.
 */
function reduced_forms_admin_form($form, &$form_state) {
  $form['reduced_forms_ids'] = array(
    '#type' => 'textarea',
    '#title' => t('Form CSS IDs'),
    '#rows' => 3,
    '#cols' => 40,
    '#default_value' => variable_get('reduced_forms_ids', ''),
    '#description' => t('Enter the CSS IDs of the forms to display reduced. One per line.'),
  );

  return system_settings_form($form);
}

