-- SUMMARY --

Reduced Forms uses the "#states" property of the Drupal Forms API to
make all but on of the fields in a form invisible until a user enters something 
in the visible field. This makes it possible to have forms that take up very 
little space on a page until a user actually starts to fill in the form.

The code for this module was adapted from the Compact Forms module at:
  http://drupal.org/project/compact_forms

-- REQUIREMENTS --

* None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONFIGURATION --

* Configure forms to display reduced in Administration » Configuration »
  User interface » Reduced Forms.


-- CUSTOMIZATION --

* To programmatically disable the reduced forms behavior on a particular form,
  set the following property on the $form element in your form constructor
  function or via hook_form_alter():

    $form['#reduced_forms'] = FALSE;


-- CONTACT --

Current maintainers:
* Sheldon Rampton - http://drupal.org/user/54136

This project has been sponsored by:
* New Amsterdam Ideas (Nuams)
  Visit http://www.nuams.com for more information.

